// Require modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


// Require folders
const userRoutes = require("./routes/userRoutes")
const productRoutes = require("./routes/productRoutes")


// Create server
const app = express();
const port = 4001;

//Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Connect to MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@course-booking.ubhpmvq.mongodb.net/ecommerce-app?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set notification for connection success or failure
let db = mongoose.connection;

// Notify on Error
db.on("error", console.error.bind(console, "connection error"));
// Notify on Successfully Connected
db.once("open", () => console.log("We're connected to the cloud database."));

//Routes for our API
//localhost:4001/users
app.use("/users", userRoutes);
//localhost:4001/products
app.use("/products", productRoutes);




// Listening to port
// This syntax will allow flexibility when using the application locally or as a hosted application.
app.listen(process.env.PORT || port, () =>{
	console.log(`API is now online on port ${process.env.PORT || port}`)
})