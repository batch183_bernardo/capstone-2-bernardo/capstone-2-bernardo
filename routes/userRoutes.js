const express = require("express");
const router = express.Router();


const userControllers = require("../controllers/userControllers")

const auth = require("../auth")

//Router for checking if the email exists
router.post("/verifyEmail", (req, res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Router for the user registration
router.post("/registration", (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for all Orders (admin only)
router.get("/orders", auth.verify, (req, res)=>{

		const userData = auth.decode(req.headers.authorization);

		if(userData.isAdmin){
		userControllers.allOrders().then(resultFromController => res.send(resultFromController))
		}
		else{
			res.send ("Access denied! You have no permission page.")
			// return false			
		}
})


// Route for the user login (with token creation)
router.post("/login", (req, res) => {
	userControllers.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for all user details (admin only)
router.get("/all", (req, res)=>{
		userControllers.allUserDetails(req.params.userId, req.body).then(resultFromController => res.send(resultFromController))
})

// Route for user details
router.get("/details", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization); //contains the token 
	console.log(userData);

	// Provides the user's ID for the getProfile controller method
	userControllers.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

// Route for the admin only to set isAdmin either true/false
router.patch("/:userId", (req, res)=>{
		userControllers.setupAdmin(req.params.userId, req.body).then(resultFromController => res.send(resultFromController))		
})

// Route for non-admin user checkout
router.post("/checkout", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: userData.id,
		productId: req.body.productId,
	}
	console.log(data.productId)
	if(userData.isAdmin){
		res.send(false)
	}
	else{
	userControllers.createOrder(data).then(resultFromController => res.send (resultFromController));
	}
})


// Route for myOrders (non-admin)
router.get("/:userId/myOrders", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		res.send("For non-admin page only!")
	}
	else{
	userControllers.myOrder(req.params.userId).then(resultFromController => res.send (resultFromController));
	}
})




module.exports = router;