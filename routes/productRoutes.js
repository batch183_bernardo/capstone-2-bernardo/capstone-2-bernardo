const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");


const auth = require("../auth");

// Search product
// router.get("/search/:keyword", (req, res) => {
// 	productControllers.search(req.params.keyword.trim()).then(resultFromController => res.send(resultFromController))
// })

// Route for all product
router.get("/all", auth.verify, (req, res) =>{
	productControllers.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// Route for creating a product
router.post("/", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	
	if(userData.isAdmin){
	productControllers.addProduct(req.body).then(resultFromController => res.send (resultFromController));
	}
	else {
		res.send("Access denied! You have no permission to make changes on this page.")
		//return false
	}
})


// Route for all active product
router.get("/", (req, res) =>{
	productControllers.getAllActive().then(resultFromController => res.send(resultFromController));
})



// Route for specific product 
router.get("/:productId", (req,res)=>{
	productControllers.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})


// Route for updating the product (admin only)
router.put("/:productId", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
	productControllers.updateProduct(req.params.productId, req.body).then(resultFromController => res.send (resultFromController));
	}
	else{
		res.send("Access denied! You have no permission to make changes on this page.")
		//return false
	}
})


// Route for archiving product (admin only)
router.patch("/:productId/archive", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
	productControllers.archivingProduct(req.params.productId, req.body).then(resultFromController => res.send (resultFromController));
	}
	else{
		res.send("Access denied! You have no permission to make changes on this page.")
		//return false
	}
})





module.exports = router;