const User = require("../models/User");
const Product = require("../models/Product")
const bcrypt = require("bcrypt");
const auth = require("../auth")


// Checking emails
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			// return "This email address is already used, please sign in"
			return true;
		}
		else{
			// return "Proceed to registration page"
			return false;
		}
	});
}

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	});

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			console.log(user);
			return true;
		}
	})
}

// Login User
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null){
			// return "Incorrect Email or Password"
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}		
			else{
				// return "Incorrect Email or Password"
				return false;
			}
		}
	})
}
// All user details (Admin Only)
module.exports.allUserDetails = () => {
	return User.find({}).then(result => result);
}

// Specific User Details
module.exports.getProfile = (data) =>{
	console.log(data)
	return User.findById(data.userId).then(result =>{
		result.password ="";

		return result;
	})
}

// Set-up isAdmin
module.exports.setupAdmin = (userId, reqBody) =>{
	
		let updateAdminField = {
			isAdmin: reqBody.isAdmin
		};

	return User.findByIdAndUpdate(userId, updateAdminField).then((adminUpdate, error) => {
		if(error){
			// return "You don't have permission in this page."
			return false
		}
		else{
			// return "Your account has successfully updated!"
			return true
		}
	})
}

// Non - admin user checkout
module.exports.createOrder = async (data) => {

	const productPrice = await Product.findById(data.productId).then(product => product.price)


	let isUserUpdated = await User.findById(data.userId).then(user =>{

		const orderCreate = {
			productId: data.productId,
			totalAmount: productPrice
		}

		user.purchasedProduct.push(orderCreate);

		return user.save().then((purchased, error)=>{
			if(error){
				return false;
			}
			else {
				return true;
			}
		})
	})

	console.log (isUserUpdated);


	let isProductUpdated = await Product.findById(data.productId).then(product =>{

		product.purchaser.push({userId: data.userId})

		product.stocks -= 1

		if(product.stocks < 0){
			return false
		}
		else{

			return product.save().then((purchaser, error) =>{
				if(error){
					return false;
				}
				else{
					return true
				}

			})
		}	
	})

	console.log(isProductUpdated);

	if(isUserUpdated && isProductUpdated){
		return true;
	}
	else{
		return false;
	}
}


module.exports.myOrder = (userId) =>{
	return User.findById(userId).then(result => result);

}



// all orders (admin only)
module.exports.allOrders = () => {
	return Product.find({}).then(result => result);
}