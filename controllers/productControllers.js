const Product = require("../models/Product");
const auth = require("../auth")


// Product Creation
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	})

	return newProduct.save().then((product, error)=>{
		if(error){
			return false
		}
		else {
			console.log(product)
			return true
		}
	})
}

// All  Product
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => result);
}


// All active Product
module.exports.getAllActive = () => {
	return Product.find({isActive:true}).then(result => result);
}

// Specific Product
module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result => result);

}

// Update Product
module.exports.updateProduct = (productId, reqBody) =>{
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((productUpdate, error) =>{
		if(error){
			return false;
		}
		else{
			// return "Successfully Update!"
			return true;
		}
	})
}


// Archiving Product
module.exports.archivingProduct = (productId, reqBody) =>{
	
		let updateActiveField = {
			isActive: reqBody.isActive
		};

	return Product.findByIdAndUpdate(productId, updateActiveField).then((productUpdate, error) => {
		if(error){
			return false
		}
		else{
			// return "Successfully Update!"
			return true
		}
	})
}

// module.exports.search = (keyword) => {
// 	return Product.find({
// 		isActive: true,
// 		$or: [
// 			{ name: { $regex: keyword, $options: "$i" } }, 
// 			{ description: { $regex: keyword, $options: "$i" } },
// 			{ tags: { $regex: keyword, $options: "$i" } }
// 		]
// 	}).then(result => result);
// }