const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "Firstname is required"]
	},
	lastName: {
		type: String,
		required: [true, "Lastname is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},
	createdOn:{
		type: Date,
		default: new Date()
	},
	purchasedProduct: [
		{
			productId: {
				type :String,
				required: [true, "Product ID is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			},
			totalAmount:{
				type: Number,
				required: [true, "total amount is required"]
			}
		}
	]	
})


module.exports = mongoose.model("User", userSchema);